<!DOCTYPE html>
<html>
<head>
<title>GraphBot</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script language="javascript" type="text/javascript" src="resources/js/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript" src="resources/js/graphbot.js"></script>
<script language="javascript" type="text/javascript" src="resources/js/owl.carousel.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

<style type="text/css">
/* Owl Carousel */
.owl-carousel,.owl-carousel .owl-item{-webkit-tap-highlight-color:transparent;}.owl-carousel{display:none;width:100%;z-index:1}.owl-carousel .owl-stage{position:relative;-ms-touch-action:pan-Y;-moz-backface-visibility:hidden}.owl-carousel .owl-stage:after{content:".";display:block;clear:both;visibility:hidden;line-height:0;height:0}.owl-carousel .owl-stage-outer{position:relative;-webkit-transform:translate3d(0,0,0)}.owl-carousel .owl-item,.owl-carousel .owl-wrapper{-webkit-backface-visibility:hidden;-moz-backface-visibility:hidden;-ms-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0)}.owl-carousel .owl-item{min-height:1px;float:left;-webkit-backface-visibility:hidden;-webkit-touch-callout:none}.owl-carousel .owl-item img{display:block;width:100%}.owl-carousel .owl-dots.disabled,.owl-carousel .owl-nav.disabled{display:none}.no-js .owl-carousel,.owl-carousel.owl-loaded{display:block}.owl-carousel .owl-dot,.owl-carousel .owl-nav .owl-next,.owl-carousel .owl-nav .owl-prev{cursor:pointer;cursor:hand;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel.owl-loading{opacity:0;display:block}.owl-carousel.owl-hidden{opacity:0}.owl-carousel.owl-refresh .owl-item{visibility:hidden}.owl-carousel.owl-drag .owl-item{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel.owl-grab{cursor:move;cursor:grab}.owl-carousel.owl-rtl{direction:rtl}.owl-carousel.owl-rtl .owl-item{float:right}.owl-carousel .animated{animation-duration:1s;animation-fill-mode:both}.owl-carousel .owl-animated-in{z-index:0}.owl-carousel .owl-animated-out{z-index:1}.owl-carousel .fadeOut{animation-name:fadeOut}@keyframes fadeOut{0%{opacity:1}100%{opacity:0}}.owl-height{transition:height .5s ease-in-out}.owl-carousel .owl-item .owl-lazy{opacity:0;transition:opacity .4s ease}.owl-carousel .owl-item img.owl-lazy{transform-style:preserve-3d}.owl-carousel .owl-video-wrapper{position:relative;height:100%;background:#000}.owl-carousel .owl-video-play-icon{position:absolute;height:80px;width:80px;left:50%;top:50%;margin-left:-40px;margin-top:-40px;background:url(owl.video.play.png) no-repeat;cursor:pointer;z-index:1;-webkit-backface-visibility:hidden;transition:transform .1s ease}.owl-carousel .owl-video-play-icon:hover{-ms-transform:scale(1.3,1.3);transform:scale(1.3,1.3)}.owl-carousel .owl-video-playing .owl-video-play-icon,.owl-carousel .owl-video-playing .owl-video-tn{display:none}.owl-carousel .owl-video-tn{opacity:0;height:100%;background-position:center center;background-repeat:no-repeat;background-size:contain;transition:opacity .4s ease}.owl-carousel .owl-video-frame{position:relative;z-index:1;height:100%;width:100%}

* { margin:0; padding:0}
*, *:before, *:after {  -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;}
.cf:before, .cf:after{content:" ";display:table}
.cf:after{clear:both}
body{color:#222;font-family: 'Open Sans', sans-serif;font-size:14px;line-height:1.5; overflow:hidden; max-width:480px; margin:0 auto;}
#content{width:100%;}
#action-space{display:inline;float:left;padding-left:20px;}
#history{width:100%;height:calc(100vh - 50px); background-color:#f6f6fc; overflow-y:scroll;padding:10px 0 0;}
.human-speech, .bot-speech{clear:both;padding:0;}
.human-speech > .inner, .bot-speech > .inner{padding:8px 15px;max-width:calc(90% - 20px);text-align:left;margin:7px 10px; position:relative;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px; border:1px solid transparent; color:#1d1d1f}
.human-speech > .inner{float:right;background-color:#d1dee1;}
.bot-speech > .inner{float:left;background-color:#fff;border-color:#e6eaeb}
.btn { display:block; text-align:center; padding:5px 0;}
.btn a { display:inline-block; background-color:#0096ff; border:2px solid #0096ff; color:#fff; font-size:15px; font-weight:bold;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius:5px; padding:5px 20px; text-decoration:none;}
.btn a:hover { background-color:#fff; color:#0096ff;}
#entry {position:relative;width:100%;padding:0;background-color:#fff;border-top:1px solid #e0e0e0;}
#user-says{width:100%;background-color:#fff; color:#1d1d1f; border:none; font-size:14px; padding:10px 60px 10px 15px; height:50px}
#send-button {display:block; width:35px; height:35px; background:url(resources/images/btn-submit.png) no-repeat center center; position:absolute; right:15px; top:50%; margin-top:-18px;overflow: hidden;text-indent: -1000px; cursor:pointer}
.itemSlider {clear:both; overflow:hidden; position:relative}
.itemSlider a { text-decoration:none;}
.itemSlider .owl-carousel { width:75%; }
.itemSlider .owl-carousel .item {-webkit-border-radius:10px;-moz-border-radius:10px;border-radius:10px; -webkit-box-shadow: 0px 0px 15px 0px rgba(141, 141, 172, 0.3);-moz-box-shadow:0px 0px 15px 0px rgba(141, 141, 172, 0.3);box-shadow:0px 0px 15px 0px rgba(141, 141, 172, 0.3); border:1px solid #eee; overflow:hidden; padding-bottom:15px; margin:15px; background-color:#fff}
.itemSlider .img { margin-bottom:10px;}
.itemSlider a:hover .img { opacity:0.8}
.itemSlider h2 { font-size:16px; color:#1d1d1f; font-weight:bold; padding:0 15px 15px; line-height:normal; min-height:62px}
.itemSlider .source { display:block; padding:0 15px; color:#666}
.bot-speech {position:relative; padding-left:45px}
.bot-speech:before {content:""; width:40px; height:40px; background:url(resources/images/ico-bot.png) no-repeat center center;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%; background-size:100% 100%; position:absolute; left:8px; top:7px; z-index:9}
.owl-carousel .owl-nav .owl-next, .owl-carousel .owl-nav .owl-prev { position:absolute; top:15px; background:url(resources/images/arrow.png) no-repeat; width:30px; height:155px; overflow:hidden; text-indent:-1000px; background-size:auto 40px; opacity:0.2; z-index:99}
.owl-carousel .owl-nav .owl-next.disabled, .owl-carousel .owl-nav .owl-prev.disabled { display:none}
.owl-carousel:hover .owl-nav .owl-next, .owl-carousel:hover .owl-nav .owl-prev { opacity:0.8} 
.owl-carousel .owl-nav .owl-prev { background-position:0 center; left:25px;}
.owl-carousel .owl-nav .owl-next {background-position:right center; right:10px}
</style>
</head>
<body>
<div id="content">
  <div id="history"></div>
  <div id="entry">
    <input type="text" id="user-says" placeholder="Enter your message"><a id="send-button" href="javascript:void(0)" >send</a>
  </div>
</div>
<div id="action-space"></div>
</body>
</html>
