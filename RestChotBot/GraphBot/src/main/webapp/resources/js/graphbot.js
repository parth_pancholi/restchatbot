
// Simple action call - just show an image
function drawGraph()
{
   $('#action-space').html("<img src='resources/images/graph.png' style='transform:scale(0.9)' />");
}

function initSlider(){
	if($('.owl-carousel').length > 0){
		$('.owl-carousel').owlCarousel({
			loop:false,
			margin:0,
			nav:true,
			dots:false,
			items:1
		})
	}
}

// Send user text back to the server ...
function sendMessage(msgtxt)
{
   $.ajax({
      type: "POST",
      data: { action:'usersays', text:msgtxt },
      cache: false,
      async: true,
      timeout: 30000,
      dataType: "json",
      headers: { 'Accept': 'application/json' },
      success: function(json, textStatus, jqXHR) {
         console.log(json);

         // Add the chat-bot message to the history area
         $('#history').append($("<div class='bot-speech'><div class='inner'>" + json.text + "</div></div>"));
         initSlider();
         // Has an action been specified?
         if (json.action)
         {
        	if(json.action === "searchRecipe"){
        		console.log("Custom action called.");
        		//call the intent for finding the recipe again to the user.
        		sendMessage("need another recipe");
        	}
            // Yes .. can we map it to a javascript function?
            var fn = window[doc.action];
            if (typeof fn === 'function')
            {
               // Yes, so invoke it ... need to consider arguments
               fn();
            }
         }
      },
      error:function(e) {
         alert("error");
      }
   });
}

// On initialise ...
$(document).ready(function() {

   // Send an initialise message to the chat-bot
   sendMessage("");

   // On 'enter' key ...
   $('#user-says').keypress(function (e) {
      var key = e.which;
      if (key == 13)  // 'enter key' code
      {
         // Get the user message
         var msgtxt = $(this).val();

         // Ignore blank message texts
         if (msgtxt.match(/^\s{0,}$/)) return(false);

         // Send the message to the chat-bot
         sendMessage(msgtxt);

         // Clear the text entry box
         $(this).val("");

         // Add the user message to the history area
         $('#history').append($("<div class='human-speech'><div class='inner'>" + msgtxt + "</div></div>"));
         
         return(false);
      }
   });
   $('#send-button').click(function (e) {
	      console.log("button clicked");
	   // Get the user message
	         var msgtxt = $("#user-says").val();

	         // Ignore blank message texts
	         if (msgtxt.match(/^\s{0,}$/)) return(false);

	         // Send the message to the chat-bot
	         sendMessage(msgtxt);

	         // Clear the text entry box
	         $("#user-says").val("");

	         // Add the user message to the history area
	         $('#history').append($("<div class='human-speech'><div class='inner'>" + msgtxt + "</div></div>"));

	         return(false);
	   });
});
