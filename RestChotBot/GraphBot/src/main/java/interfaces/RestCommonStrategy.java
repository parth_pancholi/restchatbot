package interfaces;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.log4j.Logger;

import com.graph.bot.util.constants.Constant;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.utils.RestDefaultConstantsConfiguration;

// TODO: Auto-generated Javadoc
/**
 * The Interface RestCommonStrategy.
 *
 * @author aspire106
 */
public interface RestCommonStrategy {
	final static Logger log = Logger.getLogger(RestCommonStrategy.class);

  /**
   * Intilize default configuration.
   *
   * @param connectionTimeout the connection timeout
   * @param socketTimeout the socket timeout
   * @param maxCacheEntries the max cache entries
   * @param maxObjectSize the max object size
   * @param isSharedCache the is shared cache
   * @param connectTimeout the connect timeout
   * @param requestConfigSocketTimeout the request config socket timeout
   * @param hostName the host name
   */
  default void intilizeDefaultConfiguration(Long connectionTimeout, Long socketTimeout,
      Integer maxCacheEntries, Long maxObjectSize, Boolean isSharedCache, Integer connectTimeout,
      Integer requestConfigSocketTimeout, String hostName) {
    RestDefaultConstantsConfiguration.setConnectionTimeout(connectionTimeout);
    RestDefaultConstantsConfiguration.setSocketTimeout(socketTimeout);
    RestDefaultConstantsConfiguration.setMaxCacheEntries(maxCacheEntries);
    RestDefaultConstantsConfiguration.setMaxObjectSize(maxObjectSize);
    RestDefaultConstantsConfiguration.setSharedCache(isSharedCache);
    RestDefaultConstantsConfiguration.setConnectTimeout(connectTimeout);
    RestDefaultConstantsConfiguration.setRequestConfigSocketTimeout(requestConfigSocketTimeout);
    RestDefaultConstantsConfiguration.setHostName(hostName);
  }

  /**
   * Intilize unirest basic configuration. The parameter will give the option to user for the
   * default initlization.
   *
   * @param initDefaultConfig the init default config
   */
  default void intilizeUnirestBasicConfiguration(boolean initDefaultConfig) {
    // TODO get all the above parameters from the database table where it has been set.
    if (initDefaultConfig) {
      intilizeDefaultConfiguration(null, null, null, null, null, null, null, null);
    }
    // Set the default time out for all the requests.
    Unirest.setTimeouts(RestDefaultConstantsConfiguration.getConnectionTimeout(),
        RestDefaultConstantsConfiguration.getSocketTimeout());
    // Set the default headers for all the requests.
    Unirest.setDefaultHeader("Accept", "application/json");
    Unirest.setDefaultHeader("Accept-Encoding", "gzip");
    Unirest.setDefaultHeader("Content-Type", "application/json");
    // Set the default caching implementation for the unirest api.
    CacheConfig cacheConfig = CacheConfig.custom()
        .setMaxCacheEntries(RestDefaultConstantsConfiguration.getMaxCacheEntries())
        .setMaxObjectSize(RestDefaultConstantsConfiguration.getMaxObjectSize())
        .setSharedCache(RestDefaultConstantsConfiguration.isSharedCache()).build();
    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(RestDefaultConstantsConfiguration.getConnectTimeout())
        .setSocketTimeout(RestDefaultConstantsConfiguration.getRequestConfigSocketTimeout())
        .build();
    CloseableHttpClient cachingClient = CachingHttpClients.custom().setCacheConfig(cacheConfig)
        .setDefaultRequestConfig(requestConfig).build();
    Unirest.setHttpClient(cachingClient);
  }


  /**
   * Post method call with fields.
   *
   * @param url the url
   * @param headers the headers
   * @param fields the fields
   * @return the completable future
   */
  default Future<HttpResponse<JsonNode>> postMethodCallWithFields(String url,
      Map<String, String> headers, Map<String, Object> fields) {
    return Unirest.post(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .fields(fields).asJsonAsync();
  }


  /**
   * Post method call with body.
   *
   * @param url the url
   * @param headers the headers
   * @param postBody the post body
   * @return the completable future
   */
  default Future<HttpResponse<JsonNode>> postMethodCallWithBody(String url,
      Map<String, String> headers, JsonNode postBody) {
    return Unirest.post(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .body(postBody).asJsonAsync();
  }

  /**
   * Response handling for all code.
   *
   * @param response the response
   * @return the json node
   */
  default JsonNode responseHandlingForAllCode(HttpResponse<JsonNode> response) {
    switch (response.getStatus()) {
      case 200:
        return responseHandlingFor200Code(response);
      case 400:
        return responseHandlingFor400Code();
      case 401:
        return responseHandlingFor401Code();
      case 403:
        return responseHandlingFor403Code();
      case 404:
        return responseHandlingFor404Code();
      case 408:
        return responseHandlingFor408Code();
      case 419:
        return responseHandlingFor419Code();
      case 500:
        return responseHandlingFor500Code();
      case 501:
        return responseHandlingFor501Code();
      case 503:
        return responseHandlingFor503Code();
      case 504:
        return responseHandlingFor504Code();
      case 520:
      case 521:
      case 522:
      case 523:
      case 524:
        return responseHandlingFor52Code();
      default:
        return null;
    }
  }

  /**
   * Response handling for 200 code.
   *
   * @param response the response
   * @return the json node
   */
  default JsonNode responseHandlingFor200Code(HttpResponse<JsonNode> response) {
    return response.getBody();
  }

  /**
   * Response handling for 400 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor400Code() {
    return new JsonNode("[{\"Status\" : \"400\" ,"
        + "\"message\" : \"The server wasn\'t able to process your request. Please try again. (400)\"}]");
  }

  /**
   * Response handling for 401 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor401Code() {
    return new JsonNode("[{\"Status\" : \"401\" ,"
        + "\"message\" : \"You have entered an invalid email address and/or password. Please try again.\"}]");
  }

  /**
   * Response handling for 403 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor403Code() {
    return new JsonNode("[{\"Status\" : \"403\" ,"
        + "\"message\" : \"You are not authorized to perform this action.\"}]");
  }

  /**
   * Response handling for 404 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor404Code() {
    return new JsonNode("[{\"Status\" : \"404\" ,"
        + "\"message\" : \"The endpoint is not valid, or a resource represented by the request does not exist. (404)\"}]");
  }

  /**
   * Response handling for 408 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor408Code() {
    return new JsonNode("[{\"Status\" : \"408\" ,"
        + "\"message\" : \"The server was not able to complete your request in the time allotted."
        + " This could be due to server load, and may be retried in the future. (408)\"}]");
  }

  /**
   * Response handling for 419 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor419Code() {
    return new JsonNode("[{\"Status\" : \"419\" ,"
        + "\"message\" : \"Your session has experied. Please login again before procedding ahead.\"}]");
  }

  /**
   * Response handling for 500 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor500Code() {
    return new JsonNode("[{\"Status\" : \"500\" ,"
        + "\"message\" : \"An unhandled error has occurred, and Xpresso server has been notified. (500)\"}]");
  }

  /**
   * Response handling for 501 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor501Code() {
    return new JsonNode("[{\"Status\" : \"501\" ,"
        + "\"message\" : \"This functionality is not yet implemented. Please contact Xpresso control center admin for more information. (501)\"}]");
  }

  /**
   * Response handling for 503 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor503Code() {
    return new JsonNode("[{\"Status\" : \"503\" ,"
        + "\"message\" : \"The service is temporarily unavailable. Please try again later. (503)\"}]");
  }

  /**
   * Response handling for 504 code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor504Code() {
    return new JsonNode("[{\"Status\" : \"504\" ,"
        + "\"message\" : \"The request was unable to be processed in time. This may be due to network availability."
        + " Please try again later. (504)\"}]");
  }

  /**
   * Response handling for 52* code.
   *
   * @return the json node
   */
  static JsonNode responseHandlingFor52Code() {
    return new JsonNode("[{\"Status\" : \"520\" ,"
        + "\"message\" : \"The service is temporarily unavailable or the request was unable to be processed in time."
        + " This may be due to network availability. Please try again later. (520)\"}]");
  }

  /**
   * Options method call.
   *
   * @param url the url
   * @param headers the headers
   * @param fields the fields
   * @return the completable future
   */
  default Future<HttpResponse<JsonNode>> optionsMethodCall(String url,
      Map<String, String> headers, Map<String, Object> fields) {
    return Unirest.options(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .fields(fields).asJsonAsync();
  }


  /**
   * Gets the method call.
   *
   * @param url the url
   * @param headers the headers
   * @return the method call
   */
  default Future<HttpResponse<JsonNode>> getMethodCall(String url,
      Map<String, String> headers) {
    return Unirest.get(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .asJsonAsync();
  }


  /**
   * Put method call.
   *
   * @param url the url
   * @param headers the headers
   * @param fields the fields
   * @return the completable future
   */
  default Future<HttpResponse<JsonNode>> putMethodCall(String url,
      Map<String, String> headers, Map<String, Object> fields) {
    return Unirest.put(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .fields(fields).asJsonAsync();
  }

  /**
   * Delete method call.
   *
   * @param url the url
   * @param headers the headers
   * @param fields the fields
   * @return the completable future
   */
  default Future<HttpResponse<JsonNode>> deleteMethodCall(String url,
      Map<String, String> headers, Map<String, Object> fields) {
    return Unirest.delete(RestDefaultConstantsConfiguration.getHostName() + url).headers(headers)
        .fields(fields).asJsonAsync();
  }

  /**
   * Make rest call.
   *
   * @param methodType the method type
   * @param url the url
   * @param headers the headers
   * @param fields the fields
   * @param postBody the post body
   * @return the json node
   * @throws InterruptedException the interrupted exception
   * @throws ExecutionException the execution exception
   */
  default JsonNode makeRestCall(String methodType, String url, Map<String, String> headers,
      Map<String, Object> fields, JsonNode postBody) {
    switch (methodType) {
      case Constant.POST_WITH_Fields:
        return responseHandler(postMethodCallWithFields(url, headers, fields));
      case Constant.POST_WITH_BODY:
        return responseHandler(postMethodCallWithBody(url, headers, postBody));
      case Constant.GET:
        return responseHandler(getMethodCall(url, headers));
      case Constant.PUT:
        return responseHandler(putMethodCall(url, headers, fields));
      case Constant.OPTIONS:
        return responseHandler(optionsMethodCall(url, headers, fields));
      case Constant.DELETE:
        return responseHandler(deleteMethodCall(url, headers, fields));
      default:
        return responseHandlingFor404Code();
    }
  }

  /**
   * Response handler.
   *
   * @param future the method call
   * @return the json node
   * @throws InterruptedException the interrupted exception
   * @throws ExecutionException the execution exception
   */
  default JsonNode responseHandler(Future<HttpResponse<JsonNode>> future) {
    while (!future.isDone()) {
      log.debug("Processing the rest call make to the server..!!");
    }
    // Call the method for the default mechanism for handling all the response codes.
    try {
      return responseHandlingForAllCode(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Exception while making rest call", e);
    } finally {
      try {
        Unirest.shutdown();
      } catch (IOException e) {
        log.error("Exception Occurred while shutting down unirest.", e);
      }
    }
    return new JsonNode("[{\"message\" : \"Exception occurred while making Rest call.\"}]");
  }
}
