package com.graph.bot.util.constants;

public class Constant {
	/** The Constant DEFAULT_LANDING_PAGE_PATH. */
	  /*
	   * default.landing.page.path is set in portal-ext.properties file. Currently not set as not allows
	   * to set in component property. For Production : /group/control-center For local : /group/guest
	   * change to /group/control-center TODO use custom fields of sites instead.
	   */
	  /** The Constant DEFAULT_CONNECTION_TIMEOUT. */
	  public final static Long DEFAULT_CONNECTION_TIMEOUT = 60000L;

	  /** The Constant DEFAULT_MAX_OBJECT_SIZE. */
	  public final static Long DEFAULT_MAX_OBJECT_SIZE = 8192L;

	  /** The Constant DEFAULT_SOCKET_TIMEOUT. */
	  public final static Long DEFAULT_SOCKET_TIMEOUT = 30000L;

	  /** The Constant DEFAULT_REQUEST_CONFIG_SOCKET_TIMEOUT. */
	  public final static Integer DEFAULT_REQUEST_CONFIG_SOCKET_TIMEOUT = 30000;

	  /** The Constant DEFAULT_CONNECT_TIMEOUT. */
	  public final static Integer DEFAULT_CONNECT_TIMEOUT = 30000;

	  /** The Constant DEFAULT_MAX_CACHE_ENTRIES. */
	  public final static Integer DEFAULT_MAX_CACHE_ENTRIES = 1000;

	  /** The Constant DEFAULT_IS_SHARED_CACHE. */
	  public final static Boolean DEFAULT_IS_SHARED_CACHE = false;

	  /** The Constant DEFAULT_HOSTNAME. */
	  public final static String DEFAULT_HOSTNAME = "http://food2fork.com/";

	  /** The Constant POST_WITH_BODY. */
	  public final static String POST_WITH_BODY = "postWithBody";

	  /** The Constant POST_WITH_Fields. */
	  public final static String POST_WITH_Fields = "postWithFields";

	  /** The Constant GET. */
	  public final static String GET = "get";

	  /** The Constant PUT. */
	  public final static String PUT = "put";

	  /** The Constant OPTIONS. */
	  public final static String OPTIONS = "options";

	  /** The Constant DELETE. */
	  public final static String DELETE = "delete";

}
