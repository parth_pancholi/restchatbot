package com.utils;

import org.springframework.validation.Validator;

import com.graph.bot.util.constants.Constant;

public class RestDefaultConstantsConfiguration {
	/** The connection timeout. */
	private static Long connectionTimeout;

	private static String hostName;

	/** The socket timeout. */
	private static Long socketTimeout;

	/** The max cache entries. */
	private static Integer maxCacheEntries;

	/** The max object size. */
	private static Long maxObjectSize;

	/** The is shared cache. */
	private static Boolean isSharedCache;

	/** The connect timeout. */
	private static Integer connectTimeout;

	/** The request config socket timeout. */
	private static Integer requestConfigSocketTimeout;

	public static String getHostName() {
		return hostName;
	}

	public static void setHostName(String hostName) {
		RestDefaultConstantsConfiguration.hostName = hostName != null ? hostName : Constant.DEFAULT_HOSTNAME;
	}

	/**
	 * Gets the socket timeout.
	 *
	 * @return the socket timeout
	 */
	public static Long getSocketTimeout() {
		return socketTimeout;
	}

	/**
	 * Sets the socket timeout.
	 *
	 * @param socketTimeout
	 *            the new socket timeout
	 */
	public static void setSocketTimeout(Long socketTimeout) {
		RestDefaultConstantsConfiguration.socketTimeout = socketTimeout != null ? socketTimeout
				: Constant.DEFAULT_SOCKET_TIMEOUT;
	}

	/**
	 * Gets the max cache entries.
	 *
	 * @return the max cache entries
	 */
	public static Integer getMaxCacheEntries() {
		return maxCacheEntries;
	}

	/**
	 * Sets the max cache entries.
	 *
	 * @param maxCacheEntries
	 *            the new max cache entries
	 */
	public static void setMaxCacheEntries(Integer maxCacheEntries) {
		RestDefaultConstantsConfiguration.maxCacheEntries = maxCacheEntries != null ? maxCacheEntries
				: Constant.DEFAULT_MAX_CACHE_ENTRIES;
	}

	/**
	 * Gets the max object size.
	 *
	 * @return the max object size
	 */
	public static Long getMaxObjectSize() {
		return maxObjectSize;
	}

	/**
	 * Sets the max object size.
	 *
	 * @param maxObjectSize
	 *            the new max object size
	 */
	public static void setMaxObjectSize(Long maxObjectSize) {
		RestDefaultConstantsConfiguration.maxObjectSize = maxObjectSize != null ? maxObjectSize
				: Constant.DEFAULT_MAX_OBJECT_SIZE;
	}

	/**
	 * Checks if is shared cache.
	 *
	 * @return true, if is shared cache
	 */
	public static Boolean isSharedCache() {
		return isSharedCache;
	}

	/**
	 * Sets the shared cache.
	 *
	 * @param isSharedCache
	 *            the new shared cache
	 */
	public static void setSharedCache(Boolean isSharedCache) {
		RestDefaultConstantsConfiguration.isSharedCache = isSharedCache != null ? isSharedCache
				: Constant.DEFAULT_IS_SHARED_CACHE;
	}

	/**
	 * Gets the connect timeout.
	 *
	 * @return the connect timeout
	 */
	public static Integer getConnectTimeout() {
		return connectTimeout;
	}

	/**
	 * Sets the connect timeout.
	 *
	 * @param connectTimeout
	 *            the new connect timeout
	 */
	public static void setConnectTimeout(Integer connectTimeout) {
		RestDefaultConstantsConfiguration.connectTimeout = connectTimeout != null ? connectTimeout
				: Constant.DEFAULT_CONNECT_TIMEOUT;
	}

	/**
	 * Gets the request config socket timeout.
	 *
	 * @return the request config socket timeout
	 */
	public static Integer getRequestConfigSocketTimeout() {
		return requestConfigSocketTimeout;
	}

	/**
	 * Sets the request config socket timeout.
	 *
	 * @param requestConfigSocketTimeout
	 *            the new request config socket timeout
	 */
	public static void setRequestConfigSocketTimeout(Integer requestConfigSocketTimeout) {
		RestDefaultConstantsConfiguration.requestConfigSocketTimeout = requestConfigSocketTimeout != null
				? requestConfigSocketTimeout
				: Constant.DEFAULT_REQUEST_CONFIG_SOCKET_TIMEOUT;
	}

	/**
	 * Gets the connection timeout.
	 *
	 * @return the connection timeout
	 */
	public static Long getConnectionTimeout() {
		return connectionTimeout;
	}

	/**
	 * Sets the connection timeout.
	 *
	 * @param connectionTimeout
	 *            the new connection timeout
	 */
	public static void setConnectionTimeout(Long connectionTimeout) {
		RestDefaultConstantsConfiguration.connectionTimeout = connectionTimeout != null ? connectionTimeout
				: Constant.DEFAULT_CONNECTION_TIMEOUT;
	}
}
