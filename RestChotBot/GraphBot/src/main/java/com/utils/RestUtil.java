package com.utils;

import java.net.URLEncoder;

import com.graph.bot.util.constants.Constant;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import interfaces.RestCommonStrategy;

public class RestUtil implements RestCommonStrategy {

	/** The Constant DATA_INGESTION_MODULE_STRING. */
	public final static String REST_UTIL_MODULE_STRING = "api/";

	/** The Constant INSTANCE. */
	private static final RestCommonStrategy INSTANCE = new RestUtil();

	/** The Constant CONCURRENY_MAXTOTAL. */
	private final static Integer CONCURRENY_MAXTOTAL = 1000;

	/** The Constant CONCURRENY_MAXTOTAL_PER_ROUTE. */
	private final static Integer CONCURRENY_MAXTOTAL_PER_ROUTE = 100;

	public RestUtil() {
	}

	/**
	 * Gets the singleton.
	 *
	 * @return the singleton
	 */
	public static RestCommonStrategy getSingleton() {
		return INSTANCE;
	}

	// Define the default configuration only once when the class gets loaded.
	static {
		RestUtil.getSingleton().intilizeUnirestBasicConfiguration(true);
	}

	/**
	 * Basic data ingestion initilization.
	 */
	private static void basicDataIngestionInitilization() {
		Unirest.setConcurrency(CONCURRENY_MAXTOTAL, CONCURRENY_MAXTOTAL_PER_ROUTE);
	}

	public static JsonNode getRecipeList(String ingredients) {
		basicDataIngestionInitilization();
		return RestUtil.getSingleton().makeRestCall(Constant.GET, REST_UTIL_MODULE_STRING + "search?key=d27d4d8b9fe9b2fed2be014450cdfdb6&q="+URLEncoder.encode(ingredients), null,
				null, null);
	}
}
